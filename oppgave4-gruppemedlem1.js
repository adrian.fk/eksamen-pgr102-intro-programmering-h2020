
const MAP_GRID_HEIGHT = 3;
const MAP_GRID_WIDTH = 3;

const ICONS_TO_MATCH = 3;

const USR_ICON_PATH = "./images/tac.png";
const AI_ICON_PATH = "./images/tic.png";

const OWNER_USR = "OWNER_USR";
const OWNER_AI = "OWNER_AI";

const VIEW_STATE_WELCOME = "welcomePage";
const VIEW_STATE_GAME = "playGame";
const VIEW_STATE_WINNER = "winnerFinishPage";
const VIEW_STATE_DRAW = "drawPage"


const mainStage = document.getElementById("main");

let boardCellsFilledById = [];
let board = [[]];
let numCells = 0;
let cellCounter = 0;
let winner = "";

executeViewState(VIEW_STATE_WELCOME);

function setBoardCell(cellRow, cellColumn, owner) {
    board[cellRow][cellColumn] = owner;
}

function generateMsgScreenHtml(title, msg, btnTxt) {
    return `
				<h1>${title}</h1><br>
				${msg}<br><br><br>
				<div id="msgScreenBtn">
					${btnTxt}
				</div>
			`;
}

function msgScreenSetupBtn(btnListener) {
    const btn = document.getElementById("msgScreenBtn");
    btn.style.borderRadius = "25px";
    btn.style.color = "white";
    btn.style.padding = "10px";
    btn.style.textAlign = "center";
    btn.style.border = "1px solid white";
    if (btnListener) {
        btn.style.cursor = "pointer"
        btn.onclick = btnListener;
    }
}

function setStageWithMsg(title, msg, btnTxt, btnCallback) {
    mainStage.innerHTML = generateMsgScreenHtml(title, msg, btnTxt);
    msgScreenSetupBtn(btnCallback);
}

function setupGame(mapCellsHeight, mapCellsWidth) {
    board = [[]];
    cellCounter = 0;
    boardCellsFilledById = [];
    numCells = mapCellsWidth * mapCellsHeight;
    mainStage.innerHTML = "";
    mainStage.style.display = "grid";
    mainStage.style.gridTemplateColumns = `repeat(${mapCellsWidth}, 1fr)`;
    mainStage.style.gridTemplateRows = `repeat(${mapCellsHeight}, 1fr)`;
    for (let i = 0; i < mapCellsHeight; i++) {
        board[i] = new Array(mapCellsWidth);
        for (let j = 0; j < mapCellsWidth; j++) {
            const cellID = "" + i + j;
            mainStage.insertAdjacentHTML("beforeend", generateCellHtml(cellID));
            registerCellListener(cellID, i, j);
            setBoardCell(i, j, cellID);
        }
    }
}

function generateCellHtml(cellId) {
    return `
				<div id="cell-${cellId}" style="height: 150px; width: 150px; border: 1px solid white; cursor: pointer"></div>
			`;
}

function registerCellListener(cellId, y, x) {
    const cell = document.getElementById(`cell-${cellId}`);
    cell.onclick = () => handleCellClick(cellId, y, x);
}

function fillCell(cellID, owner) {
    const cell = document.getElementById(`cell-${cellID}`)
    switch (owner) {
        case OWNER_AI:
            cell.innerHTML = `<img src="${AI_ICON_PATH}" alt="X" style="max-width: 150px">`;
            cell.style.cursor = "auto";
            break;

        case OWNER_USR:
            cell.innerHTML = `<img src="${USR_ICON_PATH}" alt="O" style="max-width: 150px">`;
            cell.style.cursor = "auto";
            break;
    }
    boardCellsFilledById.push(cellID);
    cellCounter++;
    checkBoard();
}

function isCellFilled(cellId) {
    return boardCellsFilledById.includes(cellId);
}

function fillAiCell() {
    const cellHeight = Math.floor(Math.random() * MAP_GRID_HEIGHT);
    const cellWidth = Math.floor(Math.random() * MAP_GRID_WIDTH);

    const cellID = "" + cellHeight + cellWidth;

    if (isCellFilled(cellID)) {
        fillAiCell();
    }
    else {
        setTimeout(() => fillCell(cellID, OWNER_AI), 300);
        setBoardCell(cellHeight, cellWidth, OWNER_AI);
    }
}

function isBoardFilled() {
    return cellCounter >= numCells;
}

function displayFinishScreenDraw() {
    mainStage.style.display = "block";
    setStageWithMsg(
        "Board filled up!",
        "It's a draw",
        "Retry",
        () => executeViewState(VIEW_STATE_GAME)
    );
}

function handleCellClick(cellId, y, x) {
    if (!isCellFilled(cellId)) {
        //If cell is not filled, fill with user icon
        fillCell(cellId, OWNER_USR);
        setBoardCell(y, x, OWNER_USR);

        //Initiate AI fill
        if (!isBoardFilled()) {
            fillAiCell();
        }
        else {
            //is filled
            executeViewState(VIEW_STATE_DRAW);
        }
    }
    else {
        //Handle if cell is already filled
    }
}

function displayFinishScreen() {
    mainStage.style.display = "block";
    const winnerName = (winner === OWNER_USR) ? "You" : "The opponent";
    setStageWithMsg(
        "Congratulations!",
        `${winnerName} won this round`,
        "Replay",
        () => executeViewState(VIEW_STATE_GAME)
    )
}

function displayWelcomeScreen() {
    setStageWithMsg(
        "Welcome!",
        "Ready to play some tic tac toe?",
        "Play",
        () => executeViewState(VIEW_STATE_GAME)
    )
}

function executeViewState(viewState) {
    switch (viewState) {
        case VIEW_STATE_WINNER:
            displayFinishScreen();
            break;

        case VIEW_STATE_GAME:
            setupGame(MAP_GRID_HEIGHT, MAP_GRID_WIDTH);
            break;

        case VIEW_STATE_DRAW:
            displayFinishScreenDraw();
            break;

        case VIEW_STATE_WELCOME:
            displayWelcomeScreen();
            break;
    }
}

function checkBoard() {
    let matchFound = false;

    function match(y, x) {
        matchFound = true;
        winner = board[y][x];
    }

    /**
     * Vi behøver kun sjekke i positiv logikk her fordi en match i positiv logikk siden vi "scanner" fra
     * venstre mot høyre, så vil forekomme en "match" før man har behov for å sjekke i negativ logikk.
     **/

    function isVerticalMatch(y, x) {
        return y <= MAP_GRID_HEIGHT - ICONS_TO_MATCH && (board[y][x] === board[y + 1][x] && board[y][x] === board[y + 2][x]);
    }

    function isHorizontalMatch(x, y) {
        return x <= MAP_GRID_WIDTH - ICONS_TO_MATCH && board[y][x] === board[y][x + 1] && board[y][x] === board[y][x + 2];
    }

    function isDiagonalTopLeftAnchorMatch(y, x) {
        return y <= MAP_GRID_HEIGHT - ICONS_TO_MATCH && (board[y][x] === board[y + 1][x + 1] && board[y][x] === board[y + 2][x + 2]);
    }

    function isDiagonalBottomLeftAnchorMatch(y, x) {
        return y >= ICONS_TO_MATCH-1 && board[y][x] === board[y - 1][x + 1] && board[y][x] === board[y - 2][x + 2];
    }

    for (let y = 0; y < board.length; y++) {
        for (let x = 0; x < board[y].length; x++) {

            if (isHorizontalMatch(x, y)
                || (x <= MAP_GRID_WIDTH - ICONS_TO_MATCH &&
                        (isDiagonalBottomLeftAnchorMatch(y, x)
                            || isDiagonalTopLeftAnchorMatch(y, x))
                )
                || isVerticalMatch(y, x)) {

                match(y, x);
            }
    }
    }
    if (matchFound) {
        setTimeout(
            () => executeViewState(VIEW_STATE_WINNER),
            100

        )
    }
}
